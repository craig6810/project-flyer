<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flyer extends Model
{
	/**
	 * Fillable fields for a flyer
	 *
	 * @var array
	 */
	protected $fillable = [
		'street',
		'city',
		'price',
		'description',
		'county',
		'country',
		'postcode'
	];

	public static function LocatedAt($postcode, $street)
	{
		$street = str_replace('-', ' ', $street);

		return static::where(compact('postcode', 'street'))->firstOrFail();

	}

	public function getPriceAttribute($price)
	{
		return "&#163;" . number_format($price);
	}

	public function addPhoto(Photo $photo)
	{
		return $this->photos()->save($photo);
	}

	/**
	 * A flyer is composed by many photos.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function photos()
	{
		return $this->hasMany('App\Photo');
    }

	/**
	 * A flyer is owned by a user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function owner()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	/**
	 * Determine if a given user create the flyer.
	 *
	 * @return bool
	 */
	public function ownedBy()
	{
		return $this->user_id == $user->id;
	}
}
