<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\Facades\Image;

class Photo extends Model
{

	protected $table = 'flyer_photos';

	protected $fillable = ['path', 'thumbnail_path', 'name'];

	protected $baseDir = 'images/photos';

	/**
	 * A photo belongs to one flyer
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function flyer()
	{
		return $this->belongsTo('App\Flyer');
    }

	public static function named($name)
	{
		return (new static)->saveAs($name);
	}

	public function saveAs($name)
	{
		$this->name = sprintf("%s-%s", time(), $name); // format - current time - name
		$this->path = sprintf("%s/%s", $this->baseDir, $this->name);
		$this->thumbnail_path = sprintf("%s/tn-%s", $this->baseDir, $this->name);

		return $this;
	}

	public function move(UploadedFile $file)
	{
		$file->move($this->baseDir, $this->name);

		$this->makeThumbnail();

		return $this;
	}

	protected function makeThumbnail()
	{
		Image::make($this->path)
			->fit(200)
			->save($this->thumbnail_path);
	}
}
