<?php

namespace App\Http\Controllers\Traits;

use App\Flyer;
use Illuminate\Http\Request;

trait AuthorizedUsers {

	public function userCreatedFlyer(Request $request)
	{
		return Flyer::where([
			'postcode' => $request->postcode,
			'street' => $request->street,
			'user_id' => $this->user->id
		])->exists();
	}

	protected function unauthorized(Request $request)
	{
		if($request->ajax()) {
			return response(['message' => 'No way.'], 403);
		}

		flash('no way.');

		return redirect('/');
	}
}