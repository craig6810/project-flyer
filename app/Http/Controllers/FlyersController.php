<?php

namespace App\Http\Controllers;

use App\Http\Requests\FlyerRequest;
use Illuminate\Http\Request;
use App\Flyer;
use App\Photo;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Controllers\Traits\AuthorizedUsers;

class FlyersController extends Controller
{
    use AuthorizedUsers;

    /**
     * FlyersController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);

        parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        flash('Welcome Aboard', 'Thank you for signing up ');

        return View('flyers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  FlyerRequest  $request
     * @return Response
     */
    public function store(FlyerRequest $request)
    {
        Flyer::create($request->all());

        // flash messaging
		flash()->success('Success!', 'Your flyer has been created');

        return redirect()->back(); // temp
    }

    public function show($postcode, $street)
    {
        $flyer = Flyer::locatedAt($postcode, $street);
        return View('flyers.show', compact('flyer'));
    }

    public function addPhoto($postcode, $street, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,png,bmp,jpeg'
        ]);

        $flyer = Flyer::locatedAt($postcode, $street);

        if(! $this->userCreatedFlyer($request))
        {
            return $this->unauthorized($request);
        }

        $photo = $this->makePhoto($request->file('photo'));

        $flyer->addPhoto($photo);
    }

    protected function makePhoto(UploadedFile $file)
    {
        return Photo::named($file->getClientOriginalName())
            ->move($file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
